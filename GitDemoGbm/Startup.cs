﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GitDemoGbm.Startup))]
namespace GitDemoGbm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
